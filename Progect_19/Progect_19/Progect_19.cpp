#include <iostream>
#include <string>

using namespace std;

class Animal                                        // �������� ������������� ������
{
public:
    virtual void Voice()                            // ����������� ����� ������������� ������
    {
        cout << "text" << endl;
    }
};

class Dog : public Animal                           // �������� ���������� 
{
public:
    void Voice() override                           // ������������ ������
    {
        cout << "������ ���� \t - ���" << endl;     // ���������� ������ ������
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "����� ������ \t - ���" << endl;
    }
};

class Bird : public Animal
{
public:
    void Voice() override
    {
        cout << "����� ������� \t - �����" << endl;
    }
};

int main()
{
    setlocale(LC_ALL, "ru");

    cout << "�������� ������������ ������ ���������� � �����������:" << endl << endl;

    Dog bark;
    bark.Voice();
    
    Cat soften;
    soften.Voice();

    Bird tweet;
    tweet.Voice();
    
    cout << endl << "���������� ������ � �������������� �������:" << endl << endl;
    
    Dog* pbark = &bark;
    Cat* psoften = &soften;
    Bird* ptweet = &tweet;


    Animal* arr[3];
    {
        arr[0] = pbark;
        arr[1] = psoften;
        arr[2] = ptweet;

        for (int i = 0; i < 3; i++)
        {
            arr[i]->Voice();
        }

    }

    return 0;
}